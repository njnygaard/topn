# TopN
The `topn` solution for Chloe

## Description
>Write a program, topN, that given an arbitrarily large file and a number, N, containing individual numbers on each line (e.g. 200Gb file), will output the largest N numbers, highest first. Tell me about the run time/space complexity of it, and whether you think there's room for improvement in your approach.

## Performance
### Control
This is a comparison of the two tests operating on the same file. For the first case we generate a large random file with my utility, and then sort it using `sort -nr`. This simulates the case where we just sort the list from the file and take the first N. Assuming they use Quicksort or something, the performance here would be `O(n log n)` where n is the number of lines in the test file (not the number of top slots).

![system sort](./images/large_test_cpu.png)
- This test took: `316.56s`
```
sort -nr testdata/large_test.txt  316.56s user 131.55s system 101% cpu 7:19.40 total
```
- You can see that CPU is pegged for this operation on all cores.

### My Implementation
For my implementation, in the best case we can consider 'n' (our top number) to be constant for small values of 'n'. In these cases, we perform in `O(n*lines)`, which is `O(lines)`.
In the degenerate case where 'n' approaches the size of the file and the file is reverse sorted, I will perform at `O(lines^2)` because I compare every element to find its place in the TopN array.

I didn't work toward the degenerate case because my interpretation of the tool description leads me to believe that this is the more useful implementation.

![my test](./images/my_test_large.png)
- This test took: `18.771s`
- You can see that CPU is utilized significantly less here.

## Improvements
I think I could improve on this solution by handling the degenerate case I described. Insertion sort is very good for nearly-sorted data, it would be interesting to race an insertion sort with my method and see who returns the result first. That is, if resources are free.

## Usage
### Install
>Note: You'll need a go environment

```
go get gitlab.com/droneprime/topn
```

### Generate Test File
Generate a test file called `TEST_FILE.txt` with 100 lines.
```
go run main.go -file=TEST_FILE.txt -n=100 -t
```

### Run the test
Get the largest 10 numbers from the file you generated.
```
go run main.go -file=TEST_FILE.txt -n=10
```

### Run unit tests
Run my tests. Large test is commented out because I didn't want to upload 1.9G
```
go test
```