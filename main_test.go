package main

import (
	"bufio"
	"os"
	"strconv"
	"testing"
)

func TestSortedInsert(t *testing.T) {
	s := []int{6, 5, 4, 3, 2, 1}
	result := sortedInsert(10, s)
	result = sortedInsert(20, result)
	result = sortedInsert(1, result)
	if !compareSlice(result, []int{20, 10, 6, 5, 4, 3}) {
		t.Error("Arrays are not equal.")
	}
}

func TestLittle(t *testing.T) {
	f, err := os.Open("testdata/little_test.txt")
	defer f.Close()

	if err != nil {
		t.Error("Expected file to be there.")
	}

	scanner := bufio.NewScanner(f)
	top := make([]int, 10)
	err, top = findTopN(top, scanner)
	if err != nil {
		t.Error("Error in findTopN.")
	}

	expected := []int{19, 18, 17, 16, 15, 14, 13, 12, 11, 10}

	if !compareSlice(expected, top) {
		t.Error("Arrays are not equal.")
	}
}

func TestMedium(t *testing.T) {
	f, err := os.Open("testdata/medium_test.txt")
	sorted, err := os.Open("testdata/medium_test_sorted.txt")
	defer f.Close()
	defer sorted.Close()

	if err != nil {
		t.Error("Expected file to be there.")
	}

	sortedScanner := bufio.NewScanner(sorted)

	expected := make([]int, 10)
	for i := 0; i < len(expected); i++ {
		if sortedScanner.Scan() {
			u, err := strconv.Atoi(sortedScanner.Text())
			if err != nil {
				t.Error("Something happened scanning test data.")
			}
			expected[i] = u
		}
	}

	scanner := bufio.NewScanner(f)
	top := make([]int, 10)
	err, top = findTopN(top, scanner)
	if err != nil {
		t.Error("Error in findTopN.")
	}

	if !compareSlice(expected, top) {
		t.Error("Arrays are not equal.")
	}
}

//func TestLarge(t *testing.T) {
//	f, err := os.Open("testdata/large_test.txt")
//	sorted, err := os.Open("testdata/large_test_sorted.txt")
//	defer f.Close()
//	defer sorted.Close()
//
//	if err != nil {
//		t.Error("Expected file to be there.")
//	}
//
//	sortedScanner := bufio.NewScanner(sorted)
//
//	expected := make([]int, 10)
//	for i := 0; i < len(expected); i++ {
//		if sortedScanner.Scan() {
//			u, err := strconv.Atoi(sortedScanner.Text())
//			if err != nil {
//				t.Error("Something happened scanning test data.")
//			}
//			expected[i] = u
//		}
//	}
//
//	scanner := bufio.NewScanner(f)
//	top := make([]int, 10)
//	err, top = findTopN(top, scanner)
//	if err != nil {
//		t.Error("Error in findTopN.")
//	}
//
//	if !compareSlice(expected, top) {
//		t.Error("Arrays are not equal.")
//	}
//}

func compareSlice(a, b []int) bool {
	//log.Println("Comparing:", a, b)
	if a == nil && b == nil {
		return true
	}

	if a == nil || b == nil {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
