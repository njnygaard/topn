# Notes

I wanted a space do keep track of the development decisions I'm making.

I want to use Go because parallelized channels seem well suited to processing giant files. Ideally as streams.

I want to have some command line flags and have the ability to bounce usages that won't be useful, like no file or low numbers.

To test this, I'll need a generator that can make the type of file I want to consume. As a result, I'll have to put some restrictions on the types of files I want to consume. I'll start with unsigned integers. 

Test cases are tested against linux sorting (gotta trust that right?)
Generate a large random file, sort with `sort -nr` and test against the first 10 elements of that file.