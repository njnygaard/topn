package main

import (
	"bufio"
	"flag"
	"fmt"
	"math/rand"
	"os"
	"sort"
	"strconv"
	"time"
)

var fileName string
var n int
var testMode bool

func init() {
	flag.StringVar(&fileName, "file", "", "a file containing numbers and newlines only")
	flag.IntVar(&n, "n", 0, "the largest 'n' numbers in --file")
	flag.BoolVar(&testMode, "t", false, "test mode for file generation")
	flag.Parse()
}

// check will panic if you give it an error that isn't nil
func check(e error) {
	if e != nil {
		panic(e)
	}
}

// generateTestFile will take the n argument as a number of lines and the
// fileName as the name of the file to write.
func generateTestFile(n int, fileName string) {
	source := rand.NewSource(time.Now().UnixNano())
	random := rand.New(source)

	f, err := os.Create(fileName)
	check(err)
	defer f.Close()
	//f.Sync()
	w := bufio.NewWriter(f)

	var i int
	for i = 0; i < n; i++ {
		fmt.Fprintln(w, random.Int63())
	}

	w.Flush()

}

func main() {
	if testMode {
		fmt.Println("Generating test file...")
		generateTestFile(n, fileName)
		os.Exit(0)
	}

	if fileName == "" || n <= 0 {
		fmt.Println("Please provide both positive n and file.")
		flag.Usage()
		os.Exit(0)
	}

	f, err := os.Open(fileName)
	check(err)

	scanner := bufio.NewScanner(f)
	check(scanner.Err())

	// We don't need to declare this until we actually have the conditions.
	// Make a slice of size n
	top := make([]int, n)

	err, top = findTopN(top, scanner)
	if err != nil {
		panic(err)
	}

	fmt.Println(top)

}

// findTopN will find the highest N numbers
func findTopN(top []int, s *bufio.Scanner) (error, []int) {

	for i := 0; i < len(top); i++ {
		if s.Scan() {
			u, err := strconv.Atoi(s.Text())
			if err != nil {
				return err, []int{}
			}
			top[i] = u
		}
	}

	sort.Ints(top)
	sort.Sort(sort.Reverse(sort.IntSlice(top)))

	// At this point, the array is populated with n values.
	// If n > `wc -l TEST_FILE.txt | awk '{print $1}'` then the rest of the array will be '0'
	// Otherwise, it will be a sorted list of the first values in the file.

	// Now, for each value in the file (using the same scanner, we can replace
	// the first value in our slice that smaller than the current number.

	for s.Scan() {
		u, err := strconv.Atoi(s.Text())
		if err != nil {
			return err, []int{}
		}
		top = sortedInsert(u, top)
	}
	return nil, top
}

func sortedInsert(value int, target []int) []int {
	size := len(target)
	for i := 0; i < len(target); i++ {
		if target[i] < value {
			target = append(target, 0)
			copy(target[i+1:], target[i:])
			target[i] = value
			break
		}
	}
	return append([]int(nil), target[:size]...)
}
