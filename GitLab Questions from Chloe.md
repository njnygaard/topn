Hi Nikhil,

Thank you for your interest in working at GitLab!

I would like you to take a stab at some questions. The questions are purposefully open-ended and will allow us to get a sense of your technical knowledge and your overall approach to production engineering. Please take your time completing the questions, and when you are done just shoot an email back with your answers. If you are unable to respond within 10 days, please let me know.

### Q1
> Tell me about a technical book you read recently, why you liked it, and why I should read it.

If you are interested at all in JavaScript, I cannot recommend Kyle Simpson's "You don't know JS" series more. I had the good fortune to attend a three day course with him during the Fluent Conference in 2015. The course itself was eye-opening as to the depth I was lacking in JavaScript knowledge. Paraphrasing Kyle, "there is no other language that is more popular that fewer people know". You can ask Java programmers about decorators and synchronization within the language and expect an answer, but it is much less common to encounter JS people employing prototypical inheritance in the wild.

After the conference I went and got his books. They are [free to download][1] and that is pretty much reason enough to get them. I really enjoy his teaching style and it comes through in the books. He explains topics slowly (at length) and makes sure to emphasize when he suggests re-reading. I might be slow, but I find that specifically with technical reading, re-reading is the unfortunate key to my success. I can recall the section on closure as being particularly elucidating. As a programmer, you know what closure is, but you might not have a name for it. Once I covered the topic with these books, it was like discovering gravity. I could see it everywhere, not just in JS. This was the missing key to understanding the utility of higher-order functions for me. 

One nitpick: I really hate the usage of example variable names `foo`, `bar`, `biz`, `baz`. These are a just offensive to me for their lack of effort and Kyle makes extensive use of them. I feel like there are millions of dangling `foo` pointers in my brain. >:(

[1]: https://github.com/getify/You-Dont-Know-JS

### Q2
>A service daemon in production has stopped responding to network requests. You receive an alert about the health of the service, and log in to the affected node to troubleshoot. How would you gather more information about the process and what it is doing? What are common reasons a process might appear to be locked up, and how would you rule out each possibility?

In this scenario, I assume we are getting alerts generated from some service, let's take sfx (SignalFX) as an example. Say this node is spitting data out to sfx via its collectd daemon, and we've setup this alert to trigger from some threshold in sfx. I would think at this point that we have a lovely sfx dashboard setup for this class of node (because we've taken the time to configure alerts). I'd like to browse that to see any obvious indications on the graphs. Spike in network connections, huge number of database connections, number of nodes alive. All of these situations would be really easy to see in a visual representation.

If that fails, I'll assume the alert came with a hostname for me. So now if ssh works, that rules out a lot of low level stuff. If ssh didn't work, I think I would just have to bounce the box (or roll a new instance) from the (let's say) AWS UI. My experience is with AWS, so my answers will be skewed with that perspective. Spinning up a new box would hopefully restore the service but that comes at the price of potentially losing logs. Hopefully the box was outputting to a logging platform for post-mortem.

So with all that out of the way, we can get to what's wrong with the node at the application level. I've personally had problems with collectd not sending data because of hard-coded parameters in the configuration. If the box can't talk to the reporting service, it will appear dead. So I could check the logs for that service. 

`htop` is usually included in our launch configuration, that's a great place to start to see if this is a lock with our application. If this we're a node app, we would see something like `22037 nik      20   0  629M 24124 17376 S  100.0  0.1  0:00.00 node` for a stuck node process. Hopefully we have some logging architecture for our application or a node process manager like `pm2`. With `pm2` we can look at logs and restart processes, I'm  a real fan for using this with node apps. 

- Other than that we can check for disk usage. Depending on the application for this node, like sftp, we could get service alerts if the box has no more storage.
- I really like working with systemd so if I were familiar enough with our environment, and we've written systemd daemons for our application `sudo service --status-all` would let me know if our app is still running. `journalctl -u [OFFENDING SERVICE]` should give me a bunch of information about why we're in this state.

### Q3
>A user on an ubuntu machine runs `curl http://gitlab.com` Please describe in as much detail as you can the lifecycle of the command and what happens in the kernel, over the network, and on GitLab servers before the command completes.

I think I should approach this question without research because I wouldn't have that luxury in a face-to-face interview. Hopefully that's not a dumb decision.

The `curl` utility is available out-of-the box on Ubuntu and interfaces with `libcurl`. `libcurl` presents an api that abstracts interactions with the kernel for network requests. 

The first thing that would need to happen is DNS resolution. We need to turn the hostname `gitlab.com` into an IP. If I were running it on the command line `dig gitlab.com` would give me this answer from the DNS servers I've configured:
```
;; ANSWER SECTION:
gitlab.com.     32  IN  A   52.167.219.168
```
I assume there is a system library for this that `curl` uses, I can't imagine they call `dig` and parse. 

We know we want to connect to port 80 because of `http://` and now we have the IP so `libcurl` can create the TCP connection. Our `curl` command uses the default setting `GET`, and it will build its request based on that. There is a TCP handshake orchestration next which I am not confident enough to describe. I believe it takes 4+ roundtrips to `GET` something.

Taking the luxury to actually run the command, we get a 301, redirecting us to `https://gitlab.com/`. If we were using a browser and not `curl` our browser would automatically `GET` from the new URL and cache this 301 for the future. 

From the open TCP connection, across the wire, we have a destination and a port and routing hardware takes care of establishing the round-trip from there. I won't pretend to understand the intricacies there. 

On the other end, the gitlab server is typically running a webserver like nginx which uses libraries to create listeners for ports. I imagine writing a listener is like registering a callback with the kernel and when an appropriate message comes in, the server's listener code is called.

From there, the server can build the `GET` response as it sees fit. In this case, with a 301 and a `Content-length: 0` header.

### Q4
>One of the current challenges we are facing lies in storage capacity. Describe in as much detail as you can how you would face this challenge, where do you think the main bottlenecks could be and finally what actions would you take to understand the problem and to finally deliver an infrastructure that would support our growth.

The first part of understanding this problem involves understanding the existing solution. The fact that there is a problem, means that there is a solution that isn't meeting requirements. I would first try to understand the current solution. Are we experiencing issues with locally storing files and logs? Could this solution be moved to (AWS world) EBS volumes that can be created and mounted as needed?

I think the accepted solution to this problem is a distributed filesystem. If I were researching this problem, I would start there once we identified data volume as the problem. With Hadoop, HDFS is designed for this. It is a distributed filesystem that is designed to account for hardware failures. I am not familiar with the configuration personally, but I think I could easily investigate and build a scalable CloudFormation template that could characterize a small cluster suitable for proof-of-concept and later scaling. I think there are other DFS solutions that I would research as well.

Thinking about the shape of the data on gitlab, we have many users that have many repositories. username:repository provides us with a uniquely identifiable namespace for each repository. I think this is suitable for a 'flat' structure of git repositories in a DFS. With block storage considerations relegated to a technology like HDFS, we have reduced the complexity of our solution by trusting an industry standard.

As an aside, the first thing that comes to mind as a Front-End engineer is S3. My instinct is to adopt this as a first pass to new storage challenges because it is very easy to work with. My reservation with S3 is that the pricing scales not only with storage, but transfers. git repositories slant towards `PUT` operations so this could get expensive. 

### Q5
>Write a program, topN, that given an arbitrarily large file and a number, N, containing individual numbers on each line (e.g. 200Gb file), will output the largest N numbers, highest first. Tell me about the run time/space complexity of it, and whether you think there's room for improvement in your approach.

Please see: https://gitlab.com/droneprime/topn
The questions you asked are documented in the `readme.md`.

### Other questions related to your candidacy
>On what kind of timeline are you looking for a new job?

I am actively looking for a new opportunity. GitLab has been high on my list since I started using the service. I originally started the interview process for a front-end team, but I had another offer on the table by the time I was able to interview. Working at Symphony afforded me the opportunity to work in the SRE role, and I realized that this is the role where I stand to grow the most as an engineer.

>Do you have remote working experience?

I usually work at least one day a week from home, preferably two. I find that working from home allows me to tackle larger tasks by deep diving. I am excited about the prospect of working from home exclusively because of the lack of context switches. 

>Do you have an open source project that you own or contributed to that you feel particularly proud about? could you provide a link to it or send a code sample?

Nothing open source. I have a side project that I am trying to monetize, but my team is not willing to share that until we can establish market fit. A large bulk of my front-end experience comes from working at GE and they own all that code.


